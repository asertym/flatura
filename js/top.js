$(document).ready(function() {   
    // Animate the scroll to top
    $('.go-to').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    })
});